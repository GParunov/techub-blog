import React from 'react'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Navigation from './components/Navigation/navigation'
import PopularArticle from './components/PopularArticle/PopularArticle'
import Home from './pages/Home'
import About from './pages/About'
import ArticlesDetailed from './pages/ArticlesDetailed'
import ArticlesList from './pages/ArticlesList'
import ContactUs from './pages/ContactUs'
import UserLogin from './pages/UserLogin'
import UserRegister from './pages/UserRegister'
import UserProfile from './pages/UserProfile'
import Sidebar from "./components/Sidebar/Sidebar"
import SidebarItem from "./components/Sidebar-item/SidebarItem"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'


library.add(faStar)



export default class App extends React.Component {
    render(){
        return(
            
                <Router>
                    <Route path = '/' exact component={Home}/>
                    <Route path = '/about'  component={About}/>
                    <Route path = '/detailed'  component={ArticlesDetailed}/>
                    <Route path = '/articles'  component={ArticlesList}/>
                    <Route path = '/contact'  component={ContactUs}/>
                    <Route path = '/user/login'  component = {UserLogin}/>
                    <Route path = '/user/profile'  component = {UserProfile}/>
                    <Route path = '/user/register'  component = {UserRegister}/>
                </Router>
                
               
                
            )
    }
}
