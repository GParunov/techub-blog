import React from 'react';
import Header from '../components/Header/Header'
import Navigation from '../components/Navigation/navigation'
import Jumbotron from '../components/Jumbotron/JumbotronMain'
import FuturedList from '../components/Featured-List/Featured-List'
import  './style/Home.style.css'

export default class Home extends React.Component {
    render(){
        return(
            <div>
                <div>
                    <div cla>
                        <Header className='header'/>
                    </div>
                    <div className='navigationContainer'>
                        <Navigation className='navigation'/>
                    </div>
                </div>
                <div className='jumbotronContainer'>
                    <Jumbotron className='jumbotron'/>
                </div>
                <div className='futuredListContainer'>
                    <div className='futuredlistCenter'>
                        <FuturedList/>
                    </div>
                </div>
            </div>
        )
    }
}