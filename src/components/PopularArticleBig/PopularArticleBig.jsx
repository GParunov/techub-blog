import React from "react";
import "./PopularArticleBig.css";


export default class PopularArticleBig extends React.Component {

    render() {
        return <div>

            <div className="PopArB">
                <div className="PopImg">
                    <img src="https://cdn-images-1.medium.com/focal/832/302/51/29/1*AGqTHFgSrImO29BcE4rZ9w.jpeg" className="img" alt="asd"></img>
                </div>

                <div className="PopThead">
                    <h2>Thoughts on Libra "Blockchain"</h2>
                    <p className="PopThead-p">An expert guide to the social media company’s foray into cryptocurrency</p>
                </div>
                <div className="PopTbot">
                    <p >Jameson Loop in OneZero</p>
                    <p className="PopTbot-p"> Jun 18 * 13 min read </p>
                </div>
            </div>




        </div>
    }
}