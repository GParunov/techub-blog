import React from 'react';
import "./List-Item.css";
import Popapi from '../Popup/Popup'

export class ListItem extends React.Component {
    
    render() {
        const pics = {
            image: "https://cdn-images-1.medium.com/focal/152/156/58/51/1*i49ILAkVs9oTuH_pIQzipA.jpeg"
        }
        
        return (
            <div className='featuredArticle'>
                <div className='articleTitle'>
                    <h2 className='artTitle'>Why Can’t Congress Do More to Protect Migrant Children?</h2>
                    <span className="artText">At a recent rally, Bernie Sanders touted FDR as a Socialist icon. The only problem: the 32nd president was an unabashed capitalist.</span>
                    <div className='artAuthor'>
                        <div className='author'>
                            <span className="authName">
                                <a href='#' className="authName"><Popapi /></a> in GEN</span>
                            <div className='artProp'>
                                <span className='time timeAfter'>half an hour ago</span>
                                <span className='time'>5 min read</span>
                                <span>
                                    <svg className='starSvg'>
                                        <path d="M7.438 2.324c.034-.099.09-.099.123 0l1.2 3.53a.29.29 0 0 0 .26.19h3.884c.11 0 .127.049.038.111L9.8 8.327a.271.271 0 0 0-.099.291l1.2 3.53c.034.1-.011.131-.098.069l-3.142-2.18a.303.303 0 0 0-.32 0l-3.145 2.182c-.087.06-.132.03-.099-.068l1.2-3.53a.271.271 0 0 0-.098-.292L2.056 6.146c-.087-.06-.071-.112.038-.112h3.884a.29.29 0 0 0 .26-.19l1.2-3.52z"></path>
                                    </svg>
                                </span>
                                <a href=""></a>
                            </div>
                        </div>
                        <button className='btn'>
                            <svg className='btnSvg'>
                                <path d="M19 6c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 16.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V6zm-6.838 9.97L7 19.636V6c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v13.637l-5.162-3.668a.49.49 0 0 0-.676 0z" fill-rule="evenodd"></path>
                            </svg>
                        </button>
                    </div>
                </div>
                <div className='articleImage'>
                    <img
                        src={pics.image}
                        alt="img"
                        width="152px" />
                </div>
                
            </div>

        )
    }
}