import React from "react";
import "./PopularArticle.css"
import PopularArticleBig from "../PopularArticleBig/PopularArticleBig"
import PopularArticleMid from "../PopularArticleMid/PopularArticleMid"
import PopularArticleItem from "../PopularArticle-item/PopularArticleItem"



export default class PopularArticle extends React.Component {

    render() {
        return <div>


            <article className="PopArticle">
                
                <PopularArticleBig />
                <PopularArticleMid />

           </article>


        </div>
    }
}