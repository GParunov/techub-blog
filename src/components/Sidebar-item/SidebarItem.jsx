import React from "react";
import "./SidebarItem.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {SidebarPopup} from "../Popup-Sidebar/Popup-sidebar";




class SidebarItem extends React.Component {
  
  
  render() {
     let items=[{
      name : "Jenny Gritters",
      title : "Medium member since 2019",
      text : "Privacy Technology Fellow at the Human Rights Foundation. Previously built cryptocurrency exchange platforms and blockchains at Cinnober/Nasdaq.",
          }
        ,{
          name : "wazzup",
          title : "modi aqa" ,
          text : "com"
        }]
         
         
        const datvi = items.map(item => {
       return <SidebarPopup items={item} />
     })
     
    return (
      
      
     
      <React.Fragment>
        <div className="sidebar-item">
          <div className="number-left">
          <span className="number">01</span>
          </div>
          <div className ="text-column">
          <h1 className="sidebar-item-text">
            Vanished: Future Technology
          </h1>
          <span className="names">
          <SidebarPopup /> in Elemental
          </span>
          <span className="dates">
            Jun18 - 7 min read <FontAwesomeIcon icon="star" />
          </span>
          </div>
        </div>

        <div className="sidebar-item">
          <div className="number-left">
          <span className="number">01</span>
          </div>
          <div className ="text-column">
          <h1 className="sidebar-item-text">
          What You Should Know About Granola Bars
          </h1>
          <span className="names">
            {datvi} in Elemental
          </span>
          <span className="dates">
            Jun18 - 7 min read <FontAwesomeIcon icon="star" />
          </span>
          </div>
        </div>

        <div className="sidebar-item">
          <div className="number-left">
          <span className="number">01</span>
          </div>
          <div className ="text-column">
          <h1 className="sidebar-item-text">
          What You Should Know About Granola Bars
          </h1>
          <span className="names">
            {datvi} in Elemental
          </span>
          <span className="dates">
            Jun18 - 7 min read <FontAwesomeIcon icon="star" />
          </span>
          </div>
        </div>

        <div className="sidebar-item">
          <div className="number-left">
          <span className="number">01</span>
          </div>
          <div className ="text-column">
          <h1 className="sidebar-item-text">
          What You Should Know About Granola Bars
          </h1>
          <span className="names">
            {datvi} in Elemental
          </span>
          <span className="dates">
            Jun18 - 7 min read <FontAwesomeIcon icon="star" />
          </span>
          </div>
        </div>

      </React.Fragment>
      
     
    );
  }
}

export default SidebarItem;

