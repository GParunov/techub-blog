import React from "react";
import { ListItem } from "../List-Item/List-Item";
import "./Featured-List.css";

export default class FeaturedList extends React.Component {
  render() {
    return (
      <div className="featuredList">
        <div className="featuredHeader">
          <span className="featuredHeaderTitle">Featured for members</span>
          <div className="headerTitleMore">
            <span className="titleMore">more</span>
            <span>
              <svg className="titleSvg">
                <path
                  d="M7.6 5.138L12.03 9.5 7.6 13.862l-.554-.554L10.854 9.5 7.046 5.692"
                  fill-rule="evenodd"
                />
              </svg>
            </span>
          </div>
        </div>
        <ListItem />
        <ListItem />
        <ListItem />
        <ListItem />
      </div>
    );
  }
}
