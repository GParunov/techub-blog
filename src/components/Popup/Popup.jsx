import React from 'react';
import Popup from 'reactjs-popup'
import "./Popup.css";

export default class Popapi extends React.Component {
    render() {

        const Card = ({ title }) => (
            <div className="card">
                <div className="header">
                    <div>
                        <h4 className='cardHeadName'>John DeVore</h4>
                        <p className='cardHeadSince'>Medium member since Jan 2019</p>
                    </div> 
                </div>
                <div className="content">
                    <span className='contentText'>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit autem
                        sapiente labore architecto exercitationem optio quod dolor cupiditate
                    </span>
                    
                </div>
                <div className='footer'>
                    <span className='footFoll'>Followed by15K people</span>
                    <button className='footBtn'>Follow</button>
                </div>
            </div>
          )

        const ToolTipPositions = () => (
            <div className="example-warper">
                <Popup
                    trigger={<p> John DeVore </p>}
                    position="right center"
                    on="hover"
                >
                    <Card />
                </Popup>
            </div>
            )

        return (
        <ToolTipPositions />
            
        )
    }
}