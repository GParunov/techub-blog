import React from 'react';
import getBack from './img/getBack.png';
import Search from './img/Search.png';
import facebook from './img/facebook.png';

class OpenButtonGetStarted extends React.Component {
        
     getClose() {
        var getStartedback = document.getElementById('getStartedback');

        if (getStartedback.style.display === "flex") {
            getStartedback.style.display = "none";
            }
        };

    render() {
      return (  
        <div className="signInOpen" id="getStartedback" style={{display:"none"}}>
            <div className="signInOpen_back ">
                <div className="background_img">
                    <img className="signInOpen_back_img" src={getBack} alt=""></img>
                    <button className="signInClose" onClick={this.getClose}>X</button>
                </div>
                <div className="personalizedStory">
                    <div className="welcome">
                        <h1 className="welcome_header Join_Medium_header">Join Medium.</h1> 
                        <p className="singIn_information">Create an account to receive great stories in your inbox, personalize your homepage, and follow authors and topics that you love.</p>
                    </div>
                    <div className="accounts">
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={Search} alt=""></img>
                            <p>Sign in with Google</p>
                        </button>
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={facebook} alt=""></img>
                            <p>Sign in with Facebook</p>           
                        </button>
                    </div>
                    <div className="noAccounts">
                        <p className="IfNoAccount Already_have_account">Already have an account?<a className="createOne" href="#">Sign in</a> </p>
                        <p className="sing_in_footer">To make Medium work, we log user data and share it with service providers. Click “Sign up” above to accept Medium’s  <a className="sing_in_footer" href="#">Terms of Service </a> & <a className="sing_in_footer" href="#">Privacy Policy</a> .</p>
                    </div>
                </div>
            </div>
        </div>

      );
    }
  }
  export default OpenButtonGetStarted

