import React from 'react';
import signInBack from './img/signInBack.png';
import Search from './img/Search.png';
import Twitter from './img/Twitter.png';
import Email from './img/Email.png';
import facebook from './img/facebook.png';

class OpenButtonGetStarted extends React.Component {
        
    SignClose() {
        var signInback = document.getElementById('signInback');

        if (signInback.style.display === "flex") {
            signInback.style.display = "none";
            }
        };

    render() {
      return (  
        <div className="signInOpen" id="signInback" style={{display:"none"}}>
            <div className="signInOpen_back ">
                <div className="background_img">
                    <img className="signInOpen_back_img" src={signInBack} alt=""></img>
                    <button className="signInClose" onClick={this.SignClose}>X</button>
                </div>
                <div className="personalizedStory">
                    <div className="welcome">
                        <h1 className="welcome_header ">Welcome back.</h1> 
                        <p className="singIn_information">Sign in to get personalized story recommendations, follow authors and topics you love, and interact with stories.</p>
                    </div>
                    <div className="accounts">
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={Search} alt=""></img>
                            <p>Sign in with Google</p>
                        </button>
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={facebook} alt=""></img>
                            <p>Sign in with Facebook</p>           
                        </button>
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={Twitter} alt=""></img>
                            <p>Sign in with Twitter</p>           
                        </button>
                        <button className="accauntsButton">
                            <img className="acaunts_logo" src={Email} alt=""></img>
                            <p>Sign in with Email</p>           
                        </button>
                    </div>
                    <div className="noAccounts">
                        <p className="IfNoAccount">No account? <a className="createOne" href="#">Create One</a> </p>
                        <p className="sing_in_footer">To make Medium work, we log user data and share it with service providers. Click “Sign up” above to accept Medium’s  <a className="sing_in_footer" href="#">Terms of Service </a> & <a className="sing_in_footer" href="#">Privacy Policy</a> .</p>
                    </div>
                </div>
            </div>
        </div>

      );
    }
  }

  export default OpenButtonGetStarted

