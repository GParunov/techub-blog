import React from 'react';
import './Header.css';
import logo from './img/logo.png';
import OpenButtonGetStarted from './GetStarted';
import OpenButtonSingIn from './SignIn';

class SignInDiv extends React.Component {
  getOpen() {
    var getStartedback = document.getElementById('getStartedback');

    if (getStartedback.style.display === "none") {
        getStartedback.style.display = "flex";
    }
  };
  signOpen() {
    var signInback = document.getElementById('signInback');

    if (signInback.style.display === "none") {
        signInback.style.display = "flex";
    }
  };
  render() {
    return (  
    <div className="sign_in_Avatar">
      <a href="#" className="becomeMember">Become a member </a>
      <button className="signIn" id="signIn" onClick={this.signOpen} >Sign in </button>
      <button className="getStarted" onClick={this.getOpen}>Get started</button>
    </div> 
    );
  }
}

class Search extends React.Component {
  myFunction() {
    var butSearch = document.getElementById("openMenuButton");
    var aside = document.getElementById("asd");
  
    if (aside.style.display === "none" &&  butSearch.style.marginLeft ==="200px") {
        aside.style.display = "block"
        butSearch.style.marginLeft = ''; 
    } else {
        aside.style.display = "none"
        butSearch.style.marginLeft = "200px";
    }
  };

  render() {
    return (
      <div className="ButtonSearch">
          <button className="openSearch" id="openMenuButton" style={{marginLeft:"200px"}} onClick={this.myFunction}>
              <i  className="fa fa-search"></i>
          </button>
          <input className="search" id="asd" type="text" placeholder="Search Medium"  style={{display:"none"}} />
      </div>
    );
   }
}

class MetabarBlock extends React.Component {
  render() {
    return (
      <div className="metabar_block">
        <Search />
      <SignInDiv />
      </div>
    );
  }
}
class Logo extends React.Component {
  render() {
    return (
      <div className="logo">
      <img className="header_logo" src={logo} alt="Logo" />
   </div>
    );
  }
}
class SmallHeader extends React.Component {
  render() {
    return (
      <div className="smallHeader">
      <Logo />
      <MetabarBlock />
      </div>
    );
  }
}

export default class Header extends React.Component {
  render() {
    return (
      <div className='headerContainer'>
        <div className="Header">
          <SmallHeader />
          <OpenButtonSingIn/>
          <OpenButtonGetStarted/>
      </div>
      </div>
    );
  }
}