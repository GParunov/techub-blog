import React from "react";
import "./Sidebar.css";
import SidebarItem from "../Sidebar-item/SidebarItem";


class Sidebar extends React.Component {
    render() {
        return(
    <div className="main">
        <div className ="wrapper">
            <div className="sidebar-right">
                <h1 className="sidebar-text">Popular on Medium</h1>
                
             </div>
        </div>
        <SidebarItem />
    </div>
            
        )
    }
}


export default Sidebar;