import React from "react";
import Popup from "reactjs-popup";
import "./Popup-Sidebar.css";



const contentStyle = {
  width: "250px",
  borderRadius: "2px"
};
export class SidebarPopup extends React.Component {
  render() {
 


    const picture = {
      photo:
        "https://cdn-images-1.medium.com/fit/c/50/50/0*AXUrRFbm80ee_7Xq.jpeg"
    };
    const Author = () => (
      <div className="pop">
        <div className="pop-header">
          <div className="photo-space">
            <img src={picture.photo} alt="Jenny" width="50px" />
          </div>
          <div className="text-space">
            <h4 className="header-name">{this.props.items.name}</h4>
            <p className="header-text">{this.props.items.title}</p>
          </div>
        </div>
        
        <div className="article">
          <span className="article-text">
            {this.props.items.text}
          </span>
        </div>
        <div className="footer">
          <span className="footer-text">Followed by 20K people</span>
          <button className="footer-btn">Follow</button>
        </div>
      </div>
    );

    const HiddenDiv = () => (
      <div className="hid">
        <Popup
          contentStyle={contentStyle}
          trigger={<span className="Jenni">{this.props.items.name}</span>}
          position="bottom center"
          on="click"
        >
          <Author />
        </Popup>
      </div>
    );

    return <HiddenDiv />;
  }
}

// export class AndySidebar extends React.Component {
//   render() {
//     const picture = {
//       photo:
//         "https://cdn-images-1.medium.com/fit/c/50/50/1*zDKGzyimQ2BBMt_IACFjOg.jpeg"
//     };
//     const AuthorAndy = () => (
//       <div className="pop">
//         <div className="pop-header">
//           <div className="photo-space">
//             <img src={picture.photo} alt="Andy" width="55px" />
//           </div>
//           <div>
//             <h4 className="header-name">Andy Bellati</h4>
//             <p className="header-text">Medium member since 2019</p>
//           </div>
//         </div>
//         <div className="article">
//           <span className="article-text">
//             Explainer of things, former editor-in-chief of Live Science and
//             Space .com, author of the science thriller “5 Days to Landfall.”
//           </span>
//         </div>
//         <div className="footer">
//           <span className="footer-text">Followed by 1.6K people</span>
//           <button className="footer-btn">Follow</button>
//         </div>
//       </div>
//     );
//     const HiddenAndy = () => (
//       <div className="hid">
//         <Popup
//           contentStyle={contentStyle}
//           trigger={<span className="Jenni">Andy Bellati</span>}
//           position="left center"
//           on="hover"
//         >
//           <AuthorAndy />
//         </Popup>
//       </div>
//     );

//     return <HiddenAndy />;
//   }
// }
// export class Matt extends React.Component {
//   render() {
//     const picture = {
//       photo:
//         "https://cdn-images-1.medium.com/fit/c/50/50/1*QkE2JAlWihat-j1D6WSGoA.jpeg"
//     };
//     const MattHiddenDiv = () => (
//       <div className="pop">
//         <div className="pop-header">
//           <div className="photo-space">
//             <img src={picture.photo} alt="Jenny" width="55px" />
//           </div>
//           <div>
//             <h4 className="header-name">Matt Richtel</h4>
//             <p className="header-text">Medium member since 2019</p>
//           </div>
//         </div>
//         <div className="article">
//           <span className="article-text">
//             neZero columnist, Peabody-nominated producer, and the author of
//             Faking It: The Lies Women Tell About Sex — And the Truths They
//             Reveal. http://luxalptraum.com
//           </span>
//         </div>
//         <div className="footer">
//           <span className="footer-text">Followed by 4.6K people</span>
//           <button className="footer-btn">Follow</button>
//         </div>
//       </div>
//     );

//     const HiddenMatt = () => (
//       <div className="hid">
//         <Popup
//           contentStyle={contentStyle}
//           trigger={<span className="Jenni">Matt Richtel</span>}
//           position="left center"
//           on="hover"
//         >
//           <MattHiddenDiv />
//         </Popup>
//       </div>
//     );

//     return <HiddenMatt />;
//   }
// }
// export class Rainesford extends React.Component {
//   render() {
//     const picture = {
//       photo: "https://miro.medium.com/fit/c/80/80/0*oxw8NBp_Td0n3l4_.png"
//     };
//     const RaineHiddenDiv = () => (
//       <div className="pop">
//         <div className="pop-header">
//           <div className="photo-space">
//             <img src={picture.photo} alt="Jenny" width="55px" />
//           </div>
//           <div>
//             <h4 className="header-name">Matt Richtel</h4>
//             <p className="header-text">Medium member since 2019</p>
//           </div>
//         </div>
//         <div className="article">
//           <span className="article-text">
//             Bestselling author of ‘Conspiracy,’ ‘Ego is the Enemy’ & ‘The
//             Obstacle Is The Way’ http://amzn.to/24qKRWR
//           </span>
//         </div>
//         <div className="footer">
//           <span className="footer-text">Followed by 89K people</span>
//           <button className="footer-btn">Follow</button>
//         </div>
//       </div>
//     );

//     const HiddenRaine = () => (
//       <div className="hid">
//         <Popup
//           contentStyle={contentStyle}
//           trigger={<span className="Jenni">Matt Richtel</span>}
//           position="top center"
//           on="hover"
//         >
//           <RaineHiddenDiv />
//         </Popup>
//       </div>
//     );

//     return <HiddenRaine />;
//   }
// }
