import React, { Component } from 'react'

export default class JumbotronText extends Component {
    render() {
        return (
            <>
            <h2>Never miss a story</h2>
            <span>Sign up for Mediums Daily Digest and get the best of Medium, tailored for you</span>
       </>
        )

    }
}
