import React from 'react'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import  './navigation.css'
{/* <style>
    @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
</style> */}

export default class Navigation extends React.Component {
    render(){
        return(   
            <nav className='navigation'> 
                <div className = 'navbar'>
                    <Link className = 'LinkItem' to='/' href = 'javascript:void(0)'>HOME</Link>
                    <Link className = 'LinkItem' to='/about' href = 'javascript:void(0)'> ABOUT </Link>
                    <Link className = 'LinkItem' to='/detailed' href = 'javascript:void(0)'>ARTICLES DETAILED </Link>
                    <Link className = 'LinkItem' to='/articles' href = 'javascript:void(0)'>ARTICLES LIST </Link>
                    <Link className = 'LinkItem' to='/contact' href = 'javascript:void(0)'>CONTACT US </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>SELF </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>TECH </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>HEATED </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>ZORA </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>DESIGN </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>CULTURE </Link>
                    <Link className = 'LinkItem' href = 'javascript:void(0)'>MORE </Link>
                </div>
            </nav>
        )
    }
}
