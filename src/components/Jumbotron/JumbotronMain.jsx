import React from "react";

import JumbotronSignUp from "..//JumbotronComponent/JumbotronSignUp";
import JumbotronText from "..//JumbotronComponent/JumbotronText";
import JumbotronButton from "..//JumbotronComponent/JumbotronButton";
import JumbotronImg from "..//JumbotronComponent/JumbotronImg";

export default class Jumbotron extends React.Component {
  render() {
    return (
      <>
        <div className="Jumbotron">
          <JumbotronImg />
          <JumbotronText />
          <JumbotronSignUp />
          <JumbotronButton />
        </div>
      </>
    );
  }
}
